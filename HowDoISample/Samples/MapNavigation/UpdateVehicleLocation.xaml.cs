﻿using System.Collections.ObjectModel;
using System.Reflection;
using ThinkGeo.Core;
using ThinkGeo.UI.Maui;

namespace HowDoISample.MapNavigation;

[XamlCompilation(XamlCompilationOptions.Compile)]
public partial class UpdateVehicleLocation
{
    private Collection<Vertex> _gpsPoints;
    private int _currentPointIndex;
    private InMemoryFeatureLayer _visitedRoutesLayer;
    private int _previousVertex;
    private Marker _vehicleMarker;
    private bool _initialized;

    public UpdateVehicleLocation()
    {
        InitializeComponent();
        Disappearing += (_, _) => MapView.OverlayDrawn -= MapView_OverlayDrawn;
    }
    private async void MapView_OnSizeChanged(object sender, EventArgs e)
    {
        if (_initialized)
            return;
        _initialized = true;

        // Add Cloud Maps as a background overlay
        var backgroundOverlay = new ThinkGeoCloudRasterMapsOverlay
        {
            ClientId = SampleKeys.ClientId,
            ClientSecret = SampleKeys.ClientSecret,
            MapType = ThinkGeoCloudRasterMapsMapType.Light_V2_X2,
            TileCache = new FileRasterTileCache(FileSystem.Current.CacheDirectory, "ThinkGeoCloudRasterMaps")
        };
        MapView.Overlays.Add("Background Maps", backgroundOverlay);

        _gpsPoints = await InitGpsData();

        // Create the Layer for the Route
        var routeLayer = GetRouteLayerFromGpsPoints(_gpsPoints);
        _visitedRoutesLayer = new InMemoryFeatureLayer();
        _visitedRoutesLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = LineStyle.CreateSimpleLineStyle(GeoColors.Green, 6, true);
        _visitedRoutesLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

        var layerOverlay = new LayerOverlay
        {
            TileType = TileType.SingleTile
        };
        layerOverlay.Layers.Add(routeLayer);
        layerOverlay.Layers.Add(_visitedRoutesLayer);
        MapView.Overlays.Add(layerOverlay);

        MapView.OverlayDrawn += MapView_OverlayDrawn;

        // Create a marker overlay to show where the vehicle is
        var markerOverlay = new SimpleMarkerOverlay();
        // Create the marker of the vehicle
        _vehicleMarker = new Marker
        {
            PositionX = _gpsPoints[0].X,
            PositionY = _gpsPoints[0].Y,
            ImageSource = (StreamImageSource)ImageSource.FromResource("HowDoISample.Resources.Images.vehicle-location.png"),
            PixelWidth = 24,
            PixelHeight = 24
        };
        markerOverlay.Markers.Add(_vehicleMarker);
        MapView.Overlays.Add(markerOverlay);

        AerialBackgroundCheckBox.CheckedChanged += async (_, args) =>
        {
            backgroundOverlay.MapType = args.Value
                ? ThinkGeoCloudRasterMapsMapType.Aerial_V2_X2
                : ThinkGeoCloudRasterMapsMapType.Light_V2_X2;
            await MapView.RefreshAsync(new[] { backgroundOverlay });
        };

        MapView.CenterPoint = new PointShape(_gpsPoints[0]);
        MapView.MapScale = 5000;
        await MapView.RefreshAsync();
    }

    private async void MapView_OverlayDrawn(object sender, OverlaysDrawnMapViewEventArgs e)
    {
        if (_currentPointIndex >= _gpsPoints.Count)
            return;

        var currentLocation = _gpsPoints[_currentPointIndex];
        var angle = GetRotationAngle(_currentPointIndex, _gpsPoints);

        UpdateVisitedRoutes(_currentPointIndex);

        // Update the markers position
        _vehicleMarker.UpdatePosition(currentLocation.X, currentLocation.Y);
        var animationSettings = new AnimationSettings
        {
            Type = SimultaneouslyDrawingCheckBox.IsChecked
                ? MapAnimationType.DrawWithAnimation
                : MapAnimationType.DrawAfterAnimation,
            Length = 2000,
            Easing = Easing.Linear
        };

        await MapView.ZoomToExtentAsync(new PointShape(currentLocation), MapView.MapScale, angle, animationSettings);
        _currentPointIndex++;
    }

    private static async Task<Collection<Vertex>> InitGpsData()
    {
        var gpsPoints = new Collection<Vertex>();

        // read the csv file from the embed resource. 
        var assembly = Assembly.GetExecutingAssembly();
        await using var stream = assembly.GetManifestResourceStream("HowDoISample.Data.Csv.vehicle-route.csv");
        if (stream == null) return null;

        // Convert GPS Points from Lat/Lon (srid:4326) to Spherical Mercator (Srid:3857), which is the projection of the base map
        var converter = new ProjectionConverter(4326, 3857);
        converter.Open();

        using var reader = new StreamReader(stream);
        while (!reader.EndOfStream)
        {
            var location = await reader.ReadLineAsync();
            if (location == null)
                continue;
            var posItems = location.Split(',');
            var lat = double.Parse(posItems[0]);
            var lon = double.Parse(posItems[1]);
            var vertexInSphericalMercator = converter.ConvertToExternalProjection(lon, lat);
            gpsPoints.Add(vertexInSphericalMercator);
        }
        converter.Close();
        return gpsPoints;
    }

    private static InMemoryFeatureLayer GetRouteLayerFromGpsPoints(Collection<Vertex> gpsPoints)
    {
        var lineShape = new LineShape();
        foreach (var vertex in gpsPoints)
        {
            lineShape.Vertices.Add(vertex);
        }

        // create the layers for the routes.
        var routeLayer = new InMemoryFeatureLayer();
        routeLayer.InternalFeatures.Add(new Feature(lineShape));
        routeLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = LineStyle.CreateSimpleLineStyle(GeoColors.Yellow, 6, true);
        routeLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

        return routeLayer;
    }

    private void UpdateVisitedRoutes(int currentPointIndex)
    {
        if (currentPointIndex == 0 || _previousVertex >= currentPointIndex) return;
        var lineShape = new LineShape();
        for (var i = _previousVertex; i <= currentPointIndex; i++)
        {
            lineShape.Vertices.Add(_gpsPoints[i]);
        }

        var lineFeature = new Feature(lineShape);
        _visitedRoutesLayer.InternalFeatures.Add(lineFeature);
        _previousVertex = currentPointIndex;
    }

    private static double GetRotationAngle(int currentIndex, IReadOnlyList<Vertex> gpsPoints)
    {
        Vertex currentLocation;
        Vertex nextLocation;

        if (currentIndex < gpsPoints.Count - 1)
        {
            currentLocation = gpsPoints[currentIndex];
            nextLocation = gpsPoints[currentIndex + 1];
        }
        else
        {
            currentLocation = gpsPoints[currentIndex - 1];
            nextLocation = gpsPoints[currentIndex];
        }

        double angle;
        if (nextLocation.X - currentLocation.X != 0)
        {
            var dx = nextLocation.X - currentLocation.X;
            var dy = nextLocation.Y - currentLocation.Y;

            angle = Math.Atan2(dx, dy) / Math.PI * 180; // get the angle in degrees 
            angle = -angle;
        }
        else
        {
            angle = nextLocation.Y - currentLocation.Y >= 0 ? 0 : 180;
        }
        return angle;
    }
}